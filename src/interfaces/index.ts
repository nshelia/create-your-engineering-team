export type IUser = {
  id: number;
  name: string;
  role?: string;
};
