import { IUser } from "../../interfaces";
import User from "./components/User";

type Props = {
  data: IUser[];
  onChangeRole: (id: number, role: string) => void;
  onDelete: (id: number) => void;
};

function UserList({ data, onChangeRole, onDelete }: Props) {
  const renderList = () => {
    return data.map((item) => (
      <User
        key={item.id + item.name}
        id={item.id}
        name={item.name}
        role={item.role}
        onDelete={onDelete}
        onChangeRole={onChangeRole}
      />
    ));
  };

  return <div className="flex flex-col mt-5">{renderList()}</div>;
}

export default UserList;
