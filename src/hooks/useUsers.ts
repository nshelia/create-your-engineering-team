import { useState } from "react";
import { IUser } from "../interfaces";
import { Octokit } from "octokit";
import { deserializeUser } from "../utils/deserialize";

const octokit = new Octokit({
  auth: "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",
});

function useUsers() {
  const [users, setUsers] = useState<IUser[]>([]);
  const [error, setError] = useState("");

  const userExists = (id: number) => {
    return users.some((item) => item.id === id);
  };

  const handleUserLoad = async (search: string) => {
    setError("");

    try {
      const { data } = await octokit.request("GET /users/{username}", {
        username: search,
      });
      const deserializedUser = deserializeUser(data);

      if (!userExists(deserializedUser.id)) {
        setUsers([...users, deserializedUser]);
      } else {
        setError("User already exists");
      }
    } catch (e: any) {
      if (e.message === "Not Found") {
        setError("User not found");
        return;
      }
      setError("Unexpected error");
    }
  };

  const handleChangeRole = (id: number, role: string) => {
    return setUsers(
      users.map((item) => {
        if (item.id === id) {
          return {
            ...item,
            role: role,
          };
        }
        return item;
      })
    );
  };

  const handleUserRemove = (id: number) => {
    return setUsers(users.filter((item) => item.id !== id));
  };

  return {
    users,
    error,
    handleUserRemove,
    handleChangeRole,
    handleUserLoad,
  };
}

export default useUsers;
