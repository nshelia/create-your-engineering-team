import { Button } from "@mantine/core";

type Props = {
  title: string;
  onClick: () => void;
};

function LoadButton({ title, onClick }: Props) {
  return (
    <div className="ml-2" onClick={onClick}>
      <Button>{title}</Button>
    </div>
  );
}

export default LoadButton;
