import { Container } from "@mantine/core";
import Layout from "../components/Layout";
import LoadUserForm from "../components/LoadUserForm";
import Heading from "../components/Heading";
import Error from "../components/Error";
import UserList from "../components/UserList";
import useUsers from "../hooks/useUsers";

function Home() {
  const { users, error, handleUserLoad, handleChangeRole, handleUserRemove } =
    useUsers();

  return (
    <Container>
      <Layout>
        <div className="flex-col">
          <Heading title="Create your engineering team" />
          <LoadUserForm onLoadUser={handleUserLoad} />
          {error && <Error title={error} />}
          <UserList
            data={users}
            onChangeRole={handleChangeRole}
            onDelete={handleUserRemove}
          />
        </div>
      </Layout>
    </Container>
  );
}

export default Home;
