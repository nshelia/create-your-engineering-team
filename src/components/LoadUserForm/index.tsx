import { Input } from "@mantine/core";
import LoadButton from "./components/LoadButton";
import React, { useState } from "react";

type Props = {
  onLoadUser: (search: string) => void;
};

function LoadUserForm({ onLoadUser }: Props) {
  const [search, setSearch] = useState("");

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  };

  const handleSubmit = () => {
    onLoadUser(search);
  };

  return (
    <div className="flex">
      <Input
        value={search}
        onChange={handleSearch}
        placeholder="Github user name"
        wrapperProps={{ style: { width: "100%" } }}
      />
      <LoadButton onClick={handleSubmit} title="Load user" />
    </div>
  );
}

export default LoadUserForm;
