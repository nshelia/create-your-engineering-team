import { IUser } from "../interfaces";

export const deserializeUser = (data: any) => {
  return {
    id: data.id,
    name: data.name,
    role: "",
  } as IUser;
};
