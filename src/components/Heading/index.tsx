import { Title } from "@mantine/core";

type Props = {
  title: string;
};

function Heading({ title }: Props) {
  return (
    <div className="block mb-10">
      <Title order={1}>{title}</Title>
    </div>
  );
}

export default Heading;
