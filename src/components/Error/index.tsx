import { Alert } from "@mantine/core";

type Props = {
  title: string;
};

function Error({ title }: Props) {
  return (
    <div className="block mt-10">
      <Alert title={"Error occurred"} color="red">
        {title}
      </Alert>
    </div>
  );
}

export default Error;
