import { Center } from "@mantine/core";

type IProps = {
  children: React.ReactNode;
};

const layoutStyles = {
  marginTop: 50,
};

function Layout({ children }: IProps) {
  return <Center style={layoutStyles}>{children}</Center>;
}

export default Layout;
