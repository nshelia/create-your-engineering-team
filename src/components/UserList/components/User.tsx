import { Card, Text, Chips, Chip, Title, Button } from "@mantine/core";

type Props = {
  id: number;
  name: string;
  role?: string;
  onChangeRole: (id: number, role: string) => void;
  onDelete: (id: number) => void;
};

const cardStyles = {
  marginBottom: 10,
};

function User({ id, name, role, onChangeRole, onDelete }: Props) {
  const handleChangeRole = (newRole: string) => {
    onChangeRole(id, newRole);
  };

  const handleDelete = () => {
    onDelete(id);
  };

  return (
    <Card shadow="sm" p="xl" style={cardStyles}>
      <Text weight={500} size="lg">
        {name}
      </Text>
      <div className="mt-2">
        <Title order={6}>Select a role</Title>
        <div className="mt-2">
          <Chips value={role} onChange={handleChangeRole}>
            <Chip value="feengineer">Frontend Enginner</Chip>
            <Chip value="beengineer">Backend Engineer</Chip>
          </Chips>
        </div>
      </div>

      <Button
        variant="light"
        color="red"
        fullWidth
        style={{ marginTop: 14 }}
        onClick={handleDelete}
      >
        Remove user
      </Button>
    </Card>
  );
}

export default User;
